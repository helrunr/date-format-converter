import shutil, os, re
# Regex that matches files with U.S. formatted dates
'''
. Text before date
. One/Two digits for month
. One/Two digits for day
. Four digits for year
. Text after date
'''
us_pattern = re.compile(r"""^(.*?)  
        ((0|1)?\d)-
        ((0|1|2|3)?\d)-
        ((19|20)\d\d)
        (.*?)$
        """,re.VERBOSE)

# Find files with dates and skip those without
for us_filename in os.listdir('.'):
    us_filename_match = us_pattern.search(us_filename)

    # Skip the files that contain no dates
    if us_filename_match == None:
        continue
    
    # Get the groups
    before  = us_filename_match.group(1)
    month   = us_filename_match.group(2)
    day     = us_filename_match.group(4)
    year    = us_filename_match.group(6)
    after   = us_filename_match.group(8)

# Euro dates
euro_filename = before + day + '-' + month + '-' + year + after

# Get paths
awd = os.path.abspath('.')
us_filename = os.path.join(awd, us_filename)
euro_filename = os.path.join(awd, euro_filename)

# Rename files
print('Renaming "%s" to "%s"...' % (us_filename, euro_filename))
shutil.move(us_filename, euro_filename)
